The program is designed to keep running until it is plugged to a power source. In the project prototype the power source is a battery able to output 5v, which power the board, the LIDAR sensors and the LEDs.
The only way to turn it off, for the moment, is to unplug the power source.
