/*
Author: Filippo Ferrario
Date: 22.09.2015
Note: Final version
*/

#define L 7
#define C 5
#define R 3
#define OutputL 2
#define InputL 3
#define OutputC 4
#define InputC 5
#define OutputR 6
#define InputR 7 
#define LedL 13
#define LedC 12
#define LedR 11
#define CalculationConsant 10.0
#define maxRange 300 //Max sensor detection range in centimeters

unsigned long pulse_width;
int pinSensorL = LedL;
int pinSensorC = LedC;
int pinSensorR = LedR;
unsigned long distanceSensors[3];
unsigned long distanceSensorL;   // Left Sensor
unsigned long distanceSensorC;   // Center Sensor
unsigned long distanceSensorR;   // Right Sensor


/*
This part is the initial setup;
The output pin are used as trigger for the pulse width modulation
The input pin are used as monitor, they are used as control for the same method (pwm)
*/
void setup() {
  Serial.begin(9600);
  pinMode(OutputL, OUTPUT); // set this pin (the pin 2) as trigger
  pinMode(InputL, INPUT); // set this pin (the pin 3) as monitor pin. Used for control
  pinMode(OutputC, OUTPUT);
  pinMode(InputC, INPUT);
  pinMode(OutputR, OUTPUT);
  pinMode(InputR, INPUT);
  pinMode(pinSensorL, OUTPUT);
  pinMode(pinSensorC, OUTPUT);
  pinMode(pinSensorR, OUTPUT);
  digitalWrite(OutputL, LOW); // set trigger LOW for continous read
  digitalWrite(OutputC, LOW);
  digitalWrite(OutputR, LOW);
}

/*
This function is used for read data from the sensor and convert it into measure.
pulseIn is a builtin function of arduino. It is well documented on the arduino website.
For computing the distance, check the pulse width modulation method on the pulsedLight documentation.
(The official site of the sensors.) 
For more information please read the report.
*/
void read_sensor(unsigned long sensorId, int p) {
  distanceSensors[p] = pulseIn(sensorId, HIGH);
  if(distanceSensors[p] != 0){
    distanceSensors[p] /= CalculationConsant;
  }
}

/*
This function is used for debuging the system. The goal of this method is to print the distance,
represented in centimeters, of each sensors. The console will display
Sensor number of the pin related to that sensor: number cm
*/
void print_sensors_values(int sensor, long distance){
  Serial.print("Sensor ");
  Serial.print(sensor);
  Serial.print(": ");
  Serial.print(distance);
  Serial.println(" cm");
}

/*
This function is used to check the distance and light the respective LED on the prototype.
by setting high and low each pin it is posible to turn on or off each LED.
*/
void distance_check(int ledPin, long distance){
  if(distance <= maxRange){
    digitalWrite(ledPin, HIGH);
  }
  else{
    digitalWrite(ledPin, LOW);
  }
}

void loop() {
  read_sensor(L, 0);
  read_sensor(C, 1);
  read_sensor(R, 2);
  distance_check(pinSensorL, distanceSensors[0]);
  distance_check(pinSensorC, distanceSensors[1]);
  distance_check(pinSensorR, distanceSensors[2]);
  delay(100);
}
